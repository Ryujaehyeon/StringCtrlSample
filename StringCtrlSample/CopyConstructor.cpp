#include "stdafx.h"
#include "CopyConstructor.h"


CopyConstructor::CopyConstructor()
{
}

CopyConstructor::CopyConstructor(const CopyConstructor & rhs)
    :m_nData(rhs.m_nData)
{
    this->m_nData = rhs.m_nData;
    cout << "CopyConstructor(const CopyConstructor &)" << endl;
}


CopyConstructor::~CopyConstructor()
{
}
