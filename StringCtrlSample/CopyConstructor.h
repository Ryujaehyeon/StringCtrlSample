#pragma once

class CopyConstructor
{
public:
    CopyConstructor();
    CopyConstructor(const CopyConstructor &rhs);
    virtual ~CopyConstructor();

public:
    int GetData(void) const { return m_nData; }
    void SetData(int nParam) { m_nData = nParam; }

private:
    int m_nData = 0;
};

