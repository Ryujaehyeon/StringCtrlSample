#include "stdafx.h"
#include "CopyConstructor_Sec.h"


CopyConstructor_Sec::CopyConstructor_Sec()
{
    //메모리 할당
    m_pnData = new int;
}

CopyConstructor_Sec::CopyConstructor_Sec(int nParam)
    :CopyConstructor_Sec()
{
    //값 대입
    *m_pnData = nParam;
}

CopyConstructor_Sec::CopyConstructor_Sec(const CopyConstructor_Sec &rhs)
    :CopyConstructor_Sec() //초기화 목록을 통한 생성자 호출
{
    cout << "CopyConstructor_Sec(const CopyConstructor_Sec&)" << endl;
    
    //이 코드는 기본 생성자에 있기때문에 초기화 목록으로 기본 생성자를 호출해서 코드를 재활용하는 방식
    //m_pnData = new int; 
    *m_pnData = *rhs.m_pnData;
}

CopyConstructor_Sec::~CopyConstructor_Sec()
{
    delete m_pnData;
}
