#pragma once
class CopyConstructor_Sec
{
public:
    CopyConstructor_Sec();
    CopyConstructor_Sec(int nParam);
    CopyConstructor_Sec(const CopyConstructor_Sec & rhs);
    virtual ~CopyConstructor_Sec();

public:
    inline int GetData(void) const 
    { 
        if(m_pnData != NULL)
            return *m_pnData; 
    }
    inline void SetData(const int &nParam) 
    { 
        *m_pnData = nParam; 
    }
    CopyConstructor_Sec& operator=(const CopyConstructor_Sec& rhs)
    {
        *m_pnData = *rhs.m_pnData;
        return *this;
    }
private:
    int *m_pnData = nullptr;

};

