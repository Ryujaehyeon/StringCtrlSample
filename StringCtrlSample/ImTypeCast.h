#pragma once
class CTestData
{
public:
    explicit CTestData(int nParam) : m_nData(nParam)
    {
        cout << "CTestData(int)" << endl;
    }

    virtual ~CTestData();

public:
    int GetData() const;
    void SetData(int nParam);

    operator int(void) { return m_nData; }
private:
    int m_nData = 0;
};

