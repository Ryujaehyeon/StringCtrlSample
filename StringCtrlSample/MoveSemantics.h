#pragma once
class CMoveSemantics
{
public:
    CMoveSemantics();;
    virtual ~CMoveSemantics();;

    CMoveSemantics(const CMoveSemantics &rhs) :m_nData(rhs.m_nData)
    {
        cout << "CMoveSemantics(const CMoveSemantics &)" << endl;

    }

    CMoveSemantics(CMoveSemantics &&rhs) :m_nData(rhs.m_nData)
    {
        cout << "CMoveSemantics(const CMoveSemantics &&)" << endl;

    }

    CMoveSemantics& operator=(const CMoveSemantics &) = default;

    int GetData() const { return m_nData; }
    void SetData(int nParam) { m_nData = nParam; }
private:
    int m_nData = 0;
};


