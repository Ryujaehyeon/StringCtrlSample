#include "stdafx.h"
#include "MyString.h"


CMyString::CMyString()
    : m_pszData(NULL)
    , m_nLength(0)
{
}

CMyString::CMyString(const CMyString& rhs)
    :CMyString()
{
    this->SetString(rhs.GetString());
}

CMyString::CMyString(const char* strParam)
    : CMyString()
{
    this->SetString(strParam);
}

CMyString::~CMyString()
{
    Release();
}

CMyString::CMyString(CMyString && rhs)
    :m_pszData(NULL)
    ,m_nLength(0)
{
    cout << "CMyString 이동 생성자 호출" << endl;
    m_pszData = rhs.m_pszData;
    m_nLength = rhs.m_nLength;

    rhs.m_pszData = NULL;
    rhs.m_nLength = 0;

}

size_t CMyString::SetString(const char* pszParam)
{
    Release();

    if (pszParam == NULL || strlen(pszParam) == 0)
        return 0;
 
    //size_t nlength = strlen(pszParam) + 1;
    size_t nLength = strlen(pszParam);

    m_pszData = new char[nLength + 1];
    //메모리사이즈
    //nLength = _msize(m_pszData);

    //선택적으로 사용할떄
    //strncpy_s(m_pszData, _msize(m_pszData), pszParam, strlen(pszParam));
    //strncpy_s(m_pszData, sizeof(char) * (nlength), pszParam, strlen(pszParam));

    strcpy_s(m_pszData, sizeof(char) * (nLength + 1), pszParam);

    m_nLength = nLength;
    return nLength;
}

const char * CMyString::GetString() const
{
    // TODO: 여기에 구현 코드 추가.
    if (m_pszData != NULL)
        return m_pszData;
    else
        return "";
}

CMyString CMyString::operator+(const CMyString & rhs)
{
    CMyString strResult(m_pszData);
    strResult.Append(rhs.GetString());

    return strResult;
}

CMyString CMyString::operator+=(const CMyString & rhs)
{
    Append(rhs.GetString());
    return *this;
}

void CMyString::Release()
{
    if (m_pszData != nullptr)
        delete[] m_pszData;

    m_pszData = nullptr;
    m_nLength = 0;
}


// 길이 반환
size_t CMyString::GetLength() const
{
    // TODO: 여기에 구현 코드 추가.
    return m_nLength;
}


size_t CMyString::Append(const char* pszParam)
{
    // TODO: 여기에 구현 코드 추가.
    if (pszParam == NULL)
    {
        return 0;
    }

	size_t nLenParam = strlen(pszParam);

    if (nLenParam == 0)
        return 0;

    if (pszParam == NULL)
    {
        SetString(pszParam);

        return m_nLength;
    }

	size_t nLenCur = m_nLength;
    
    char* pszResult = new char[nLenCur + nLenParam + 1];

    strcpy_s(pszResult, sizeof(char) * (nLenCur + 1), m_pszData);
    strcpy_s(pszResult + (sizeof(char)*nLenCur), sizeof(char)*(nLenParam + 1), pszParam);
    
    //
    Release();
    m_pszData = pszResult;
    m_nLength = nLenCur + nLenParam;
    
    return m_nLength;
}


