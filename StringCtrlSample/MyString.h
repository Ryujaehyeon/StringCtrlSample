#pragma once
class CMyString
{
public:
    CMyString();
    virtual ~CMyString();
    //복사 생성자
    CMyString(const CMyString& rhs);
    //변환 생성자
    explicit CMyString(const char* strParam);
    //이동 생성자
    CMyString(CMyString &&rhs);
private:
    // 문자열을 저장하기 위해 동적 할당한 메모리를 가리키는 포인터
    char* m_pszData;
    // 저장된 문자열의 길이
    size_t m_nLength;

public:
    size_t SetString(const char* pszParam);

    const char* GetString() const;

    CMyString& operator=(const CMyString& rhs)
    {
        //*m_pszData = *rhs.m_pszData;
        if(this != &rhs)
            this->SetString(rhs.GetString());

        return *this;
    }
    operator char*(void) const { return m_pszData; }
    
    CMyString operator+(const CMyString &rhs);
    CMyString operator+=(const CMyString &rhs);
	char operator[](int nIndex) const
	{
		cout << "operator[] const" << endl;
		return m_pszData[nIndex];
	}
	char& operator[](int nIndex)
	{
		cout << "operator[]" << endl;
		return m_pszData[nIndex];
	}

	int operator==(const CMyString &rhs)
	{
		if (m_pszData != nullptr && rhs.m_pszData != nullptr)
		{
			if (strcmp(m_pszData, rhs.m_pszData) == 0)
				return 1;
		}
		return 0;
	}
	int operator!=(const CMyString &rhs)
	{
		if (m_pszData != nullptr && rhs.m_pszData != nullptr)
		{
			if (strcmp(m_pszData, rhs.m_pszData) != 0)
				return 0;
		}
		return 1;
	}

    void Release();
    // 길이 반환
    size_t GetLength() const;
    //추가
    size_t Append(const char* pszParam);


};

