#pragma once
class COperAssignComplex
{
public:
	explicit COperAssignComplex(int nParam);
	virtual ~COperAssignComplex();

	operator int() { return *m_pnData; }

	COperAssignComplex& operator=(const COperAssignComplex &rhs)
	{
		if (this == &rhs)
			return *this;

		delete m_pnData;

		m_pnData = new int(*rhs.m_pnData);
		return *this;
	}

	COperAssignComplex& operator+=(const COperAssignComplex &rhs)
	{
		int *pnNewData = new int(*m_pnData);
		*pnNewData += *rhs.m_pnData;

		delete m_pnData;
		m_pnData = pnNewData;
		return *this;
	}
private:
	int * m_pnData = nullptr;
};

