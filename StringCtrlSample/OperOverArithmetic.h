#pragma once
class COperOverArithmetic
{
public:
    COperOverArithmetic();
    virtual ~COperOverArithmetic();

    //변환 생성자
    COperOverArithmetic(int nParam) :m_nData(nParam)
    {
        cout << "COperOverArithmetic(int)" << endl;
    }

    //복사 생성자ㅣ
    COperOverArithmetic(const COperOverArithmetic &rhs) : m_nData(rhs.m_nData)
    {
        cout << "COperOverArithmetic(const COperOverArithmetic &)" << endl;
    }

    //이동 생성자
    COperOverArithmetic(const COperOverArithmetic &&rhs) :m_nData(rhs.m_nData)
    {
        cout << "COperOverArithmetic(const COperOverArithmetic &&)" << endl;
    }

    //형변환
    operator int() { return m_nData; }

    //+
    COperOverArithmetic operator+(const COperOverArithmetic &rhs)
    {
        cout << "operator+" << endl;
        COperOverArithmetic result(0);
        result.m_nData = this->m_nData + rhs.m_nData;

        return result;
    }

    //=
    COperOverArithmetic& operator=(const COperOverArithmetic &rhs)
    {
        cout << "operator=" << endl;
        m_nData = rhs.m_nData;

        return *this;
    }
private:
    int m_nData = 0;
};

