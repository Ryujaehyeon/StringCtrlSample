#include "stdafx.h"
#include "OperOverArray.h"


CIntArray::CIntArray(int nSize)
{
	m_pnData = new int[nSize];
	memset(m_pnData, 0, sizeof(int)*nSize);
}


CIntArray::~CIntArray()
{
	delete m_pnData;
}

