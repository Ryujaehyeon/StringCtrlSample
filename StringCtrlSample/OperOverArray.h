#pragma once
class CIntArray
{
public:
	CIntArray(int nSize);
	virtual ~CIntArray();

public:
	int operator[](int nIndex) const
	{
		cout << "operator[] const" << endl;
		return m_pnData[nIndex];

	};

	int& operator[](int nIndex)
	{
		cout << "operator[]" << endl;
		return m_pnData[nIndex];
	}

private:
	int* m_pnData;
	int m_nSize;
};

