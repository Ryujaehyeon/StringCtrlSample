#pragma once
class COperOverAssign
{
public:
	explicit COperOverAssign(int nParam);
	virtual ~COperOverAssign();

	operator int() { return *m_pnData; }


	//void operator=(const COperOverAssign &rhs)
	//{
	//	if (this == &rhs)
	//		return;

	//	delete m_pnData;

	//	m_pnData = new int(*rhs.m_pnData);
	//}
	COperOverAssign& operator=(const COperOverAssign &rhs)
	{
		if (this == &rhs)
			return *this;

		delete m_pnData;

		m_pnData = new int(*rhs.m_pnData);
		return *this;
	}
private:
	int * m_pnData = nullptr;
};

