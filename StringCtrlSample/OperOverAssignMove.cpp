#include "stdafx.h"
#include "OperOverAssignMove.h"


COperOverAssignMove::COperOverAssignMove(int nParam)
{
	cout << "COperOverAssignMove(int)" << endl;
	m_pnData = new int(nParam);
}

COperOverAssignMove::~COperOverAssignMove()
{
}

COperOverAssignMove::COperOverAssignMove(const COperOverAssignMove & rhs)
{
	cout << "COperOverAssignMove(const COperOverAssignMove&)" << endl;
	m_pnData = new int(*rhs.m_pnData);
} 