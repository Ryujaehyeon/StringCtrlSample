#pragma once
class COperOverAssignMove
{
public:
	explicit COperOverAssignMove(int param);
	virtual ~COperOverAssignMove();

	COperOverAssignMove(const COperOverAssignMove &rhs);

	operator int() { return *m_pnData; }

	COperOverAssignMove operator+(const COperOverAssignMove &rhs)
	{
		return COperOverAssignMove(*m_pnData + *rhs.m_pnData);
	}

	COperOverAssignMove& operator=(COperOverAssignMove &rhs)
	{
		cout << "operator=" << endl;
		if (this == &rhs)
		{
			return *this;
		}
		delete m_pnData;
		m_pnData = new int(*rhs.m_pnData);
		return *this;
	}
	COperOverAssignMove& operator=(COperOverAssignMove &&rhs)
	{
		cout << "operator = (Move)" << endl;
		m_pnData = rhs.m_pnData;
		return *this;

	}
private:
	int *m_pnData = nullptr;

public:

};