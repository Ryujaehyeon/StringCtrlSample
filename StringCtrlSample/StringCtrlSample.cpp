// StringCtrlSample.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "MyString.h"
#pragma region MyInclude
//#include "CopyConstructor.h"
//#include "CopyConstructor_Sec.h"
//#include "ImTypeCast.h"
//#include "TempObject.h"
//#include "MoveSemantics.h"
#include "OperOverArithmetic.h"

#include "OperOverAssign.h"
#include "OperAssignComplex.h"
#include "OperOverAssignMove.h"
#include "OperOverArray.h"
#pragma endregion

#pragma region MyFunc
//CMyString TestFunc(void)
//{
//    CMyString strTest("TestFunc() return");
//    cout << strTest << endl;
//
//    return strTest;
//}
//CMoveSemantics TestFunc(int nParam)
//{
//    cout << "**CMoveSemantics() : Begin***" << endl;
//    CMoveSemantics a;
//    a.SetData(nParam);
//    cout << "**CMoveSemantics() : End*****" << endl;
//
//    return a;
//}

//void TestFunc(const CTestData &param)
//{
//    cout << "TestFunc(): " << param.GetData() << endl;
//}
//void TestFunc(const CMyString &strParam)
//{
//    cout << strParam << endl;
//}

//객체를 반환하는 함수
//CTempObject TestFunc(int nParam)
//{
//    // CTempObject 클래스 인스턴스인 a는 지역 변수다.
//    // 따라서 함수가 반환되면 소멸한다.
//    CTempObject a(nParam, (char*)("A"));
//
//    //함수 a 반환.
//    return a;
//}

//void TestFunc(const CIntArray &arrParam)
//{
//	cout << "testfunc()" << endl;
//	cout << arrParam[3] << endl;
//}

void TestFunc(const CMyString &strParam)
{
	cout << strParam[0] << endl;
	cout << strParam[strParam.GetLength() - 1] << endl;
}
#pragma endregion
int main()
{
#pragma region MyMainCode


	////////////////////////////////////
	//CMyString strData;
	//strData.SetString("");
	//strData.SetString("Hello");
	//strData.SetString("Hello2");
	//cout << strData.GetString() << endl;
	//
	//CopyConstructor MyConstruct;
	//MyConstruct.SetData(10);
	//
	//CopyConstructor MyConstruct_Sec(MyConstruct);
	//cout << MyConstruct_Sec.GetData() << endl;
	////////////////////////////////////

	////////////////////////////////////
	//CopyConstructor_Sec Source;
	//Source.SetData(10);
	//cout << Source.GetData() << endl;
	//
	//CopyConstructor_Sec Shellow(5);
	//cout << Shellow.GetData() << endl;
	//
	//Source = Shellow;
	//cout << Source.GetData() << endl;
	////////////////////////////////////

	////////////////////////////////////
	//CMyString strData, strTest;
	//strData.SetString("Hello");
	//strTest.SetString("World");
	//
	////복사생성
	//CMyString strNewData(strData);
	//cout << strNewData.GetString() << endl;
	//
	////단순 대입 연산자 호출
	//strNewData = strTest;
	//cout << strNewData.GetString() << endl;
	////////////////////////////////////

	/////////////////////////////////
	//TestFunc(5);
	/////////////////////////////////

	/////////////////////////////////
	//CTestData a(10);

	//cout << a.GetData() << endl;
	//cout << a << endl;
	//cout << (int)a << endl;
	//cout << static_cast<int>(a) << endl;

	//CTestData* pa;
	//pa = &a;
	/////////////////////////////////

	//CMyString strData("Hello");
	//::TestFunc(strData);
	//::TestFunc(CMyString("World"));
	//CTempObject b(5, (char*)"B");
	//cout << "*****Before*****" << endl;

	//b = TestFunc(10);
	//cout << "*****After*****" << endl;
	//cout << b.GetData() << endl;

	//CMoveSemantics b;
	//cout << "*Before************" << endl;
	//b = TestFunc(20);
	//cout << "*After************" << endl;
	//CMoveSemantics C(b);

	//TestFunc();
	//cout << "Begin" << endl;
	//COperOverArithmetic a(0), b(3), c(4);

	//a = b + c;

	//cout << a << endl;
	//cout << "end" << endl;

	//CMyString strLeft("Hello"), strRight("World"), strResult;

	//strResult = strLeft + strRight;
	//cout << strResult << endl;

	//cout << strLeft << endlCOperOverAssignMove;
	//strLeft += CMyString("World");
	//cout << strLeft << endl;
	//COperAssignComplex a(0), b(5);
	//a += b;
	//cout << a << endl;
	//return 0;

	//COperOverAssignMove a(0), b(2), c(5);
	//cout << "before" << endl;
	//a = b + c;

	//cout << "after" << endl;
	//cout << a << endl;

	//a = b;
	//cout << a << endl;
#pragma endregion

	//CIntArray arr(5);
	//for (int i = 0; i < 5; ++i)
	//{
	//	arr[i] = i * 10;
	//}
	//TestFunc(arr);

	//CMyString strParam("HelloWorld");
	//cout << strParam << endl;
	//TestFunc(strParam);

	CMyString strLeft("test"), strRight("String");
	if (strLeft == strRight)
		cout << "same" << endl;
	else
		cout << "different" << endl;

	if (strLeft != strRight)
		cout << "different" << endl;
	else
		cout << "same" << endl;
	
	return 0;
}


