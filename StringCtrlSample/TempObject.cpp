#include "stdafx.h"
#include <iostream>
#include "TempObject.h"


CTempObject::CTempObject(int nParam, char *pszName) : m_nData(nParam), m_pszName(pszName)
{
    pszName;
    cout << "CTempObject(int):" << m_pszName << endl;
}

CTempObject::CTempObject(const CTempObject &rhs) :m_nData(rhs.m_nData), m_pszName(rhs.m_pszName)
{
    cout << "CTempObject(const CTempObjec &): " << m_pszName << endl;
}

CTempObject::~CTempObject()
{
    cout << "~CTempObject():" << m_pszName << endl;
}

CTempObject& CTempObject::operator=(const CTempObject &rhs)
{
    cout << "operator=" << endl;

    //값은 변경하지만 이름(m_pszName)은 그대로 둔다.
    m_nData = rhs.m_nData;

    return *this;
}
