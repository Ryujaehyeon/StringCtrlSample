#pragma once

/// <summary>
/// 이름 없는 임시 객체의 생성과 소멸
/// </summary>
class CTempObject
{
    //생성자
public:
    CTempObject() = delete;

    CTempObject(int nParam, char *pszName);

    CTempObject(const CTempObject &rhs);

    virtual ~CTempObject();

    //메서드
public:
    CTempObject & operator=(const CTempObject &rhs);

    int GetData()const { return m_nData; }
    void SetData(int nParam) { m_nData = nParam; }

    //멤버 변수
private:
    int m_nData = 0;
    char *m_pszName = nullptr;


};

